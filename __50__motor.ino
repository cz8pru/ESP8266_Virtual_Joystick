#define MAX_PWM_VALUE 50

int my_min(int par_a, int par_b) {
  if (par_a<par_b) {
    return(par_a);
  }
  else {
    return(par_b);
  }
}
/* ######################################################################### */
/* ## l9110 ################################################################ */
uint16_t i_max_speed;
uint16_t i_act_speed;
/* ######################################################################### */
void analogWrite_min(int par_pin, int par_value) {
  analogWrite(par_pin, my_min(par_value, i_max_speed));
}
/* ######################################################################### */
void l9110_setup(uint16_t par_max_speed) {
  // set pins to output
  pinMode(A_IA_PIN, OUTPUT); 
  pinMode(A_IB_PIN, OUTPUT);
  pinMode(B_IA_PIN, OUTPUT);
  pinMode(B_IB_PIN, OUTPUT);
  analogWriteRange(par_max_speed);
  i_max_speed=par_max_speed;
}

void l9110_run(uint8_t par_motor, uint8_t par_direction, uint16_t par_speed) {
  uint8_t i_ia_pin;
  uint8_t i_ib_pin;
  
  switch(par_motor) {
    case 0:  
      /* LEFT */
      i_ia_pin=A_IA_PIN;
      i_ib_pin=A_IB_PIN;
      break;
    
    case 1:
      /* RIGHT */
      i_ia_pin=B_IA_PIN;
      i_ib_pin=B_IB_PIN;
      break;
    
    default:
      Serial.println("par_motor: wrong parameter\n");
      break;
  }

  switch(par_direction) {
    case 0: 
      analogWrite_min(i_ia_pin, par_speed);
      analogWrite_min(i_ib_pin, 0);
      break;
    
    case 1:
      analogWrite_min(i_ia_pin, 0);
      analogWrite_min(i_ib_pin, par_speed);
      break;
    
    default:
      Serial.println("par_motor: wrong parameter\n");
      break;
  }
  
}



/* ######################################################################### */
int get_url_param(char *par_string, uint8_t i_arg_num) {
// request:GET /position?x_axis=-33&y_rot=-16 HTTP/1.11
  char *i_index_of_eq;
  int i_return_int;

  i_index_of_eq=par_string;

  while (1) {
    i_index_of_eq = strchr(i_index_of_eq, '=');
    i_index_of_eq = &i_index_of_eq[1];
    
    if (i_arg_num==0) {
      break;
    }
    i_arg_num--;
  }
  if (i_index_of_eq == NULL) {
    Serial.println("Character '=' not found\n");
    return(0);
  }
  i_return_int=atoi(i_index_of_eq);
  return(i_return_int);
}

/* Get the channel */
#define WIFI_CHANNELS 13 
uint8_t wifi_find_free_channel(void) {
  uint8_t i_best_channel=WIFI_CHANNELS/2; /* SOmewhere in the middle */
  uint8_t i_rssi_per_ch[WIFI_CHANNELS+2]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  uint8_t i_rssi_per_ch_2nd[WIFI_CHANNELS+2];
  uint8_t i_channel;
  uint8_t i_rssi;
  uint8_t i_trial;
  
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  Serial.println("Wifi scan starts...");

  for(i_trial=0;i_trial<5;i_trial++) {
    // Scan it 5 times
    
    // WiFi.scanNetworks will return the number of networks found
    int n = WiFi.scanNetworks();
    if (n == 0) {
      Serial.println("no networks found");
      return(i_best_channel);
    }

    for (int i = 0; i < n; ++i) {
      i_channel = WiFi.channel(i);
      i_rssi = -1 * WiFi.RSSI(i); // just for calculation in positive numbers

      if ( i_rssi < i_rssi_per_ch[i_channel] ) 
        // higher sirnal - save it
        i_rssi_per_ch[i_channel]=i_rssi;
    }
    delay(10);
  }
  Serial.println("Wifi scan finished!");
  for (i_channel = 1; i_channel < WIFI_CHANNELS+1; i_channel++) {
    if(i_rssi_per_ch[i_channel]!=0xFF) {
      Serial.print("Ch: ");      
      Serial.print( i_channel);      
      Serial.print(" SNR: ");
      Serial.println(i_rssi_per_ch[i_channel]);            
    }
  }
  Serial.println();    
  
  // neighbors calculation 
  for (i_channel = 1; i_channel < WIFI_CHANNELS+1; i_channel++) {
    uint16_t i_rssi_avg;
    if(i_rssi_per_ch[i_channel]!=0xFF) {
      i_rssi_avg=i_rssi_per_ch[i_channel];
    }
    else {
      i_rssi_avg=i_rssi_per_ch[i_channel-1]+i_rssi_per_ch[i_channel]+i_rssi_per_ch[i_channel+1];
      i_rssi_avg=i_rssi_avg/3;
    }
    i_rssi_per_ch_2nd[i_channel]=i_rssi_avg;
    Serial.print("Ch: ");      
    Serial.print( i_channel);      
    Serial.print(" SNR: ");
    Serial.println(i_rssi_avg);            
  }
  Serial.println();

  // find the best channel
  uint8_t i_rssi_max=0;
  for (i_channel = 1; i_channel < WIFI_CHANNELS+1; i_channel++) {
    if(i_rssi_per_ch_2nd[i_channel]>i_rssi_max) {
      i_rssi_max=i_rssi_per_ch_2nd[i_channel];
      i_best_channel=i_channel;
    }
  }
  Serial.print("The best channel is: ");
  Serial.println(i_best_channel);
  return(i_best_channel);
}


