bool i_automatic;

const int i_sensor_pin_L=D5;
const int i_sensor_pin_R=D6;

int i_sensor_state_L;
int i_sensor_state_R;

#define LINE_FOLLOWR_SPEED_TIMEOUT 100
#define LINE_FOLLOWR_SPEED_MAX    15
#define LINE_FOLLOWR_SPEED_MIN    15

#define LINE_FOLLOWER_SPEED_INC_STEP  1
#define LINE_FOLLOWER_SPEED_DEC_STEP  1

enum e_CtrlCommand { eStop, eForward, eLeft, eRight };



int i_line_follower_speed = LINE_FOLLOWR_SPEED_MIN;
int i_line_follower_speed_last;




int i_line_follower_speed_time = LINE_FOLLOWR_SPEED_TIMEOUT;


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

void line_follower_speed_inc(void)
{
  if ( i_line_follower_speed_time >= 0)
  {
    i_line_follower_speed_time--;  
  }
  else 
  {
    i_line_follower_speed_time = LINE_FOLLOWR_SPEED_TIMEOUT;
    i_line_follower_speed=MIN(i_line_follower_speed + LINE_FOLLOWER_SPEED_INC_STEP, LINE_FOLLOWR_SPEED_MAX);
  }
}

void line_follower_speed_dec(void)
{
  if ( i_line_follower_speed_time >= 0)
  {
    i_line_follower_speed_time--;  
  }
  else 
  {
    i_line_follower_speed_time = LINE_FOLLOWR_SPEED_TIMEOUT;
    i_line_follower_speed=MAX(i_line_follower_speed - LINE_FOLLOWER_SPEED_DEC_STEP, LINE_FOLLOWR_SPEED_MIN);
  }
}



void line_follower(void)
{
  
  
  pinMode(i_sensor_pin_L, INPUT);
  pinMode(i_sensor_pin_R, INPUT);


    
  e_CtrlCommand i_ctrl_curr=eForward, i_ctrl_last=eStop;
  
  while(1) 
  {
    i_sensor_state_L = digitalRead(i_sensor_pin_L);
    i_sensor_state_R = digitalRead(i_sensor_pin_R);


    
    

    if((i_sensor_state_L == 1) && (i_sensor_state_R == 1)) {
      // STOP
      i_ctrl_curr = eStop;
    }

    if((i_sensor_state_L == 0) && (i_sensor_state_R == 0)) {
      // FORWARD
      i_ctrl_curr = eForward;
      line_follower_speed_inc();
    }

    if((i_sensor_state_L == 1) && (i_sensor_state_R == 0)) {
      i_ctrl_curr = eLeft;
      line_follower_speed_dec();
    }

    if((i_sensor_state_L == 0) && (i_sensor_state_R == 1)) {
      i_ctrl_curr = eRight;
      line_follower_speed_dec();      
    }

    
    if ((i_ctrl_curr != i_ctrl_last) || (i_line_follower_speed_last == i_line_follower_speed))
    {
      // CHANGE
      Serial.println(i_line_follower_speed);
      switch(i_ctrl_curr) {
        case eStop:
          l9110_run(0,0,0);
          l9110_run(1,0,0);
          break;
  
        case eForward:
          l9110_run(0,0,i_line_follower_speed);
          l9110_run(1,0,i_line_follower_speed);
          break;
  
        case eLeft:
          l9110_run(0,0,i_line_follower_speed);
          l9110_run(1,1,i_line_follower_speed);      
          break;
  
        case eRight:
          l9110_run(0,1,i_line_follower_speed);
          l9110_run(1,0,i_line_follower_speed);
          break;
      }
    }

    if (i_ctrl_curr == i_ctrl_last) 
    {
      // NO CHANGE
      yield();// otherwise there is system reset :(
    }
    
    i_ctrl_last = i_ctrl_curr;
    i_line_follower_speed_last = i_line_follower_speed;
  } // while(1)
}
