/* =========================================================== */
#ifdef DEBUG    
int memory;
int old_memory;
#endif    

int g_motor_r_speed=0;
int g_motor_l_speed=0;

#define MAX_CONNECTED_TIMEOUT 1000
volatile int g_connected_timeout=MAX_CONNECTED_TIMEOUT;

void loop()
{
  WiFiClient client = server.available(); 
  // wait for a client (web browser) to connect
  if (client)
  {

#ifdef DEBUG    
    memory = ESP.getFreeHeap();
    Serial.print("FreeHeap :");
    Serial.println(memory);
    Serial.print("Diff     :");
    Serial.println(old_memory-memory);
    old_memory=memory;
#endif    
    
    Serial.println("\n[Client connected]");
    digitalWrite(LED_PIN, LOW);
    
    g_connected_timeout = MAX_CONNECTED_TIMEOUT;
    
    while (client.connected())
    {
      if (g_connected_timeout == 0 ) {
        l9110_run(0,0,0);
        l9110_run(1,0,0);
        break;
      }
      g_connected_timeout--;
    
      // read line by line what the client (web browser) is requesting
      if (client.available())
      {
        client.readBytesUntil('\r', request, MAX_REQUEST);
      
        Serial.print("request:");
        Serial.println(request);
    
        if ( strstr(request,"/position") !=NULL ) {
          /* RESPONSE */ 
          client.println("HTTP/1.1 204 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("RESP<br>");
          client.println("</html>");

          g_motor_l_speed = get_url_param(request,0); /* left */
          g_motor_r_speed = get_url_param(request,1); /* right */
          
          Serial.print("Motor L: ");
          Serial.println(g_motor_l_speed);
          if (g_motor_l_speed > 0) {
            l9110_run(0,0,g_motor_l_speed);
          }
          else {
            l9110_run(0,1,-1*g_motor_l_speed);
          }

          Serial.print("Motor R: ");
          Serial.print(g_motor_r_speed);
          if (g_motor_r_speed > 0) {
            l9110_run(1,0,g_motor_r_speed);
          }
          else {
            l9110_run(1,1,-1*g_motor_r_speed);
          }

          break;
        }
        if ( strstr(request,"/ ") !=NULL ) {
          /* RESPONSE */ 
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println();
       
          /* Split the string, client.print is not capable to send so long data */
          i_tmp[SPLIT_LENGTH_PART]=0; /* end of string */
          uint16_t i_split_str_length=strlen(html_page);
          uint16_t i_split_str_begin=0;
          uint16_t i_split_str_length_part_rest = SPLIT_LENGTH_PART;
          do {
            if ( i_split_str_begin + SPLIT_LENGTH_PART > i_split_str_length) {
              i_split_str_length_part_rest = i_split_str_length - i_split_str_begin;
              i_tmp[i_split_str_length_part_rest]=0; /* end of string */
            }
            strncpy(i_tmp, &html_page[i_split_str_begin], i_split_str_length_part_rest);
            i_split_str_begin = i_split_str_begin + i_split_str_length_part_rest;
        
            client.print(i_tmp);
          } while(i_split_str_begin < i_split_str_length);
        
          client.println(""); 
          Serial.println(""); 
          
          Serial.print("HTML page contains [B]:");
          Serial.println(i_split_str_length);
          Serial.println("end");
          break;
        }
   
        /* RESPONSE */ 
        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: text/html");
        client.println("Connection: close");  // the connection will be closed after completion of the response
        client.println();
        client.println("<!DOCTYPE HTML>");
        client.println("<html>");
        client.println("RESP<br>");
        client.println("</html>");
        break;
      }
    }
    delay(1); // give the web browser time to receive the data

    // close the connection:
    client.stop();
    Serial.println("");
    Serial.println("[Client disonnected]");
    digitalWrite(LED_PIN, HIGH);
  }
}

