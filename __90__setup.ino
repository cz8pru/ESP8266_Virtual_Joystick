void setup() {
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  
  l9110_setup(MAX_PWM_VALUE);
  l9110_run(0,0,0);
  l9110_run(1,0,0);
  
  delay(1000);
  Serial.begin(115200);


  line_follower();




  

  digitalWrite(LED_PIN, LOW);
  uint8_t i_ap_ch = wifi_find_free_channel();
  digitalWrite(LED_PIN, HIGH);

  WiFi.mode(WIFI_AP);
  
  Serial.println();
  Serial.println("Configuring access point...");
  
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password, i_ap_ch);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);

  server.begin();
  Serial.println("Server started");
}


